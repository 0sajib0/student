<?php
include_once('../../vendor/autoload.php');
use App\Student\Message;
use App\Student\Student;
if(isset($_POST['firstname'])&&(!empty($_POST['firstname']))&& isset($_POST['middlename'])&&(!empty($_POST['middlename']))&&isset($_POST['lastname'])&&(!empty($_POST['lastname']))) {
    $student = new Student();
    $student->prepare($_POST);
    $student->store();
}
else
{
    Message::message("<div class=\"alert alert-success\">
  <strong>Unsuccess!</strong> Data has not been stored successfully.
</div>");
    Message::redirect('index.php');
}
